import React from "react"

import * as Scroll from 'react-scroll'

import '../styles/contenu.css'

import Carousel from './Carousel'

class Stats extends React.Component {
    constructor(props) {
      super(props);
      this.state = { width: 0, height: 0 };
      this.dropDown = this.dropDown.bind(this)
    }

    dropDown() {
      Scroll.animateScroll.scrollTo(this.state.height)
    }
  
    
    render() {

      return(


        <section class="hero is-primary">


          <div class="hero-body has-background-link">
            <div class="container">
              <section className="hero is-transparent ">
                <div className="hero-head">
                  <div className="container">
                    <h1 className="title">
                      Chiffres Clés
                    </h1>
                  </div>
                </div>
                <div className="hero-body has-text-centered">
                  <Carousel/>
                </div>
              </section>
            </div>
          </div>
        </section>
  
      )
      }
  }

export default Stats
 