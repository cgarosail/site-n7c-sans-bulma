import React from 'react';
import SwipeableViews from 'react-swipeable-views';
import { autoPlay } from 'react-swipeable-views-utils';

const AutoPlaySwipeableViews = autoPlay(SwipeableViews);

const styles = {
  root: {
    position: 'relative',
  },
  slide: {
    padding: 15,
    color: '#fff',
  },
};

class Carousel extends React.Component {

    state = {
    index: 0,
    };

    handleChangeIndex = index => {
        this.setState({
            index,
        });
    };

    render() {
        const { index } = this.state;

        return (
            <div style={styles.root}>
            <AutoPlaySwipeableViews index={index} onChangeIndex={this.handleChangeIndex}>
                <div style={Object.assign({}, styles.slide, styles.slide1)}>
                    <div class="notification is-large is-primary">
                        <p class="title">Une Statistique de ouf.</p>
                        <p class="subtitle">Top tile</p>
                    </div>
                </div>
                <div style={Object.assign({}, styles.slide, styles.slide2)}>
                    <div class="notification is-large is-danger">
                        <p class="title">Une autre</p>
                        <p class="subtitle">Top tile</p>
                    </div>
                </div>
                <div style={Object.assign({}, styles.slide, styles.slide3)}>
                    <div class="notification is-large is-warning">
                        <p class="title">La dernière Stat</p>
                        <p class="subtitle">Top tile</p>
                    </div>                    
                </div>
            </AutoPlaySwipeableViews>
            </div>
        );
    }

}
export default Carousel