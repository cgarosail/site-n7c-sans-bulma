import React from "react"

import * as Scroll from 'react-scroll'

import '../styles/contenu.css'




class PresentationJE extends React.Component {
  constructor(props) {
    super(props);
    this.state = { width: 0, height: 0 };
    this.updateWindowDimensions = this.updateWindowDimensions.bind(this);
    this.dropDown = this.dropDown.bind(this)
  }
  
  componentDidMount() {
    this.updateWindowDimensions();
    window.addEventListener('resize', this.updateWindowDimensions);
  }
  
  componentWillUnmount() {
    window.removeEventListener('resize', this.updateWindowDimensions);
  }
  
  updateWindowDimensions() {
    this.setState({ width: Math.max(1200,window.innerWidth), height: Math.max(710, window.innerHeight - 76) });
  }


  dropDown() {
    Scroll.animateScroll.scrollTo(this.state.height)
  }

  render() {
    return(
      <div className="pageContenubleu" style={{height:this.state.height, width:this.state.width}}>
          <h1 style ={{margin: "3% 1% 15%"}} >PresentationJE</h1>

          <p style={{color: "darkblue"}} >Ici je ne sais pas encore trop quoi mettre, surement ce qu'est le mouvement avec la CNJE etc... et une présentation du cadre légal un peu ? Je sais pas trop</p>

          <p style={{color: "darkblue"}}>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse vitae elit quis tortor posuere efficitur faucibus quis est. In hac habitasse platea dictumst. Nullam in arcu quis velit feugiat commodo. Sed eu sollicitudin velit. Nulla lacinia feugiat orci vel aliquam. Etiam hendrerit rutrum consequat. Morbi id bibendum enim. Suspendisse interdum magna nec blandit venenatis. Interdum et malesuada fames ac ante ipsum primis in faucibus. Morbi tempor urna erat, at tempus elit venenatis et. Sed finibus tortor ac ante luctus, vitae gravida ante mattis.
            <br/>
            Nullam nec magna in tellus ornare rutrum. Vestibulum eget pharetra magna. Sed facilisis, diam non feugiat porttitor, metus purus sodales felis, at placerat nulla massa in risus. Mauris nec cursus massa, sed consequat orci. Vivamus ipsum orci, aliquet nec tempor et, cursus vel neque. Cras vehicula tincidunt erat quis accumsan. Praesent vulputate libero eu lorem bibendum rhoncus. Duis hendrerit molestie nibh, non egestas nunc convallis vel. Aenean quis elementum odio. Phasellus risus arcu, tempor non mollis eu, cursus eu felis. Ut porttitor eros id lorem sodales pharetra. Nunc arcu nunc, sodales ac tempus ac, mattis ac lorem.

</p>
          
        
      </div>

    )
    }
}

export default PresentationJE