import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faAngleUp} from '@fortawesome/free-solid-svg-icons'
import { faFacebook, faTwitter, faLinkedin} from '@fortawesome/free-brands-svg-icons'

import * as Scroll from 'react-scroll'

import '../styles/footer.css'

class Footer extends React.Component {
    constructor(props) {
        super(props);
        this.dropUp = this.dropUp.bind(this)
    }
      
    dropUp() {
        Scroll.animateScroll.scrollToTop()
      }

    render() {
        return (
            <footer className="basPage">
                <ul className="subSection">
                    <div className="contact">
                        <h3>Contactez nous</h3>
                        <hr className="ligne"/>
                        <ul className="subSection">
                            <div style={{paddingRight: "50px"}}>
                                <p>Une question, une remarque, un devis ? Contactez nous ! Réponse sour 24h.</p>
                                <ul className="subSection" >
                                    <a className='liens' href="https://twitter.com/n7consulting"><FontAwesomeIcon size="3x"  icon={faTwitter} color="rgb(255,255,255)" /></a>
                                    <a className='liens' href="https://www.facebook.com/N7ConsultingToulouse/"><FontAwesomeIcon size="3x" icon={faFacebook} color="rgb(255,255,255)" /></a>
                                    <a className='liens' href="https://www.linkedin.com/company/n7-consulting"><FontAwesomeIcon size="3x" icon={faLinkedin} color="rgb(255,255,255)" /></a>
                                </ul>
                            </div>
                            <div>
                                <div> 2 rue Charles Camichel, 31071 Toulouse CEDEX7</div>
                                <div>+33 (0)5 34 32 20 99</div>
                                <a href="mailto:contact@n7consulting.fr" style={{color:'lightblue'}} >contact@n7consulting.fr</a>
                                <div>N°SIRET:344 162 490 00017</div>
                            </div>
                        </ul>
                    </div>
                    <div className="partenaires">
                        <h3> Partenaires</h3>
                        <hr className="ligne"/>
                        <ul>
                            <p>Amaris </p>
                            <p>Société Générale </p>
                            <p>CNJE </p>
                        </ul>
                    </div>

                    <div>
                        <button style={{background: 'inherit', border:'none', height:'100%'}}>
                            <FontAwesomeIcon size="8x" onClick={this.dropUp} icon={faAngleUp} color="rgb(255,255,255)" />
                        </button> 
                    </div>
                </ul>
            </footer>
        )
    }
}

 export default Footer