import React from 'react'
import Link from 'gatsby-link'
import '../styles/header.css'
import {
  Collapse,
  Navbar,
  NavbarToggler,
  Nav,
  //NavbarBrand,
  NavItem,
  //NavLink,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  //DropdownItem
} from 'reactstrap';


import ModalPlaquette from './ModalPlaquette'


class NavbarMenu extends React.Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      isOpen: false
    };
  }
  toggle() {
    this.setState({
      isOpen: !this.state.isOpen
    });
  }


  render() {
    const menus = this.props.siteData.options.map(option => 
      option.deroulant ? 
        <UncontrolledDropdown  nav inNavbar style={{marginBottom:'0', paddingLeft: '50px'}} key={option.title}>
          <DropdownToggle className="navbarItem" nav>{option.name}</DropdownToggle>
          <DropdownMenu style = {{backgroundColor: 'rgb(0, 0, 100)', borderRadius: '15px'}} size='sm'>
            {option.subs.map( sousMenu => <Link key={sousMenu.name} className="navbarSub" style ={{display:'flex', justifyContent:'center', margin:'5% 4% 5%'}} to={sousMenu.link}>{sousMenu.name}</Link>)}

          </DropdownMenu>
        </UncontrolledDropdown>
      :
        option.popup ?
          <ModalPlaquette key={option.name} classe="navbarItem" buttonLabel={option.name} />
        :
          <NavItem style={{marginBottom:'0'}} key={option.title}>
            <Link className="navbarItem" to={option.link}>{option.name}</Link>
          </NavItem>
        
    ) 


      return(
      <header>
        <Navbar fixed={"top"} expand="md" style={{backgroundColor: ' rgba(22,97,154,0.9)', borderRadius:'50px', margin:'10px'}}>
          <Link className="navbarItem" to="/">{this.props.siteData.title}</Link>
          <NavbarToggler onClick={this.toggle} onMouseOver={this.toggle} />
          <Collapse isOpen={this.state.isOpen} navbar>
            <Nav className="ml-auto" style ={{alignItems:'center', marginBottom:'0'}}navbar>
              {menus}
            </Nav>
          </Collapse>
        </Navbar>
      </header>

      )
    }
  } 
  export default NavbarMenu