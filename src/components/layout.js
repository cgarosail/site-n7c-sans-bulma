/**
 * Layout component that queries for data
 * with Gatsby's StaticQuery component
 *
 * See: https://www.gatsbyjs.org/docs/static-query/
 */

import React from "react"
import PropTypes from "prop-types"
import { StaticQuery, graphql } from "gatsby"

import "../styles/layout.css"



const Layout = ({ children }) => (
  <StaticQuery
    query={graphql`
      query SiteTitleQuery {
        site {
          siteMetadata {
            author
            navbar {
              title
              options {
                deroulant
                link
                name
                popup
                subs {
                  link
                  name
                }
              }
            }
            pages {
              Accueil {
                PresentationJE {
                  titre
                  paragraphes {
                    texte
                    titre
                  }
                }
                Statistiques {
                  titre
                  stats {
                    titre
                  }
                }
              }
            }
            title
          }
        }
      }
      
      
    `}
    render={data => (
        
        <div style = {{
          display: "flex",
          minHeight: "100vh",
          flexDirection: "column",
          backgroundColor: "whitesmoke"
        }}>
          <main pages={data.site.siteMetadata.pages} className="corps">{children}</main>
        </div>
    )}
  />
)

Layout.propTypes = {
  children: PropTypes.node.isRequired,
}

export default Layout
