import React from "react"

import * as Scroll from 'react-scroll'

import "../styles/index.scss"


class FirstImpression extends React.Component {
  constructor(props) {
    super(props);
    this.dropDown = this.dropDown.bind(this)
  }
  dropDown() {
    Scroll.animateScroll.scrollTo(this.state.height)
  }


  render() {

    return(

        <div className="wrapper">

          <section className="hero is-fullheight is-mobile">
            <div className="hero-head">

            <nav className="navbar is-fixed-top has-background-primary">
                <div className="container">
                  <div className="navbar-brand">
                    <a class="navbar-item" href="https://bulma.io">
                      <img src="https://bulma.io/images/bulma-logo.png" alt="Bulma: Free, open source, & modern CSS framework based on Flexbox" width="112" height="28"/>
                    </a>
                    <span className="navbar-burger burger" data-target="navbarMenuHeroA">
                      <span></span>
                      <span></span>
                      <span></span>
                    </span>
                  </div>
                  <div id="navbarMenuHeroA" className="navbar-menu">
                    <div className="navbar-end">
                      <a className="navbar-item">
                        Vous êtes ?
                      </a>
                      <a className="navbar-item">
                        L'équipe
                      </a>
                      <a className="navbar-item">
                        La plaquette
                      </a>
                      <a className="navbar-item">
                        Actualités
                      </a>
                      <a className="navbar-item">
                        Nous Contacter
                      </a>
                    </div>
                  </div>
                </div>
              </nav>   
            </div>           
            
            <div className="hero-body">
              <div className="container has-text-centered">
              <section className="hero is-transparent is-medium">
                <div className="hero-body">
                  <div className="container">
                    <h1 className="title is-size-1-desktop has-text-warning is-spaced">
                      N7 Consulting
                    </h1>
                    <h2 className="subtitle has-text-white">
                      42 ans d’expertise au service d’un monde innovant
                    </h2>
                  </div>
                </div>
              </section>
                <button className="button is-light is-medium">
                  Plaquette
                </button>
              </div>
            </div>

          </section>
        </div>
 
      )
    }
}

export default FirstImpression
