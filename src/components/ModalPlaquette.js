import React from 'react'
import 'bootstrap/dist/css/bootstrap.min.css'
import { Button, Modal, ModalHeader, ModalBody, ModalFooter, Form, Input, Label } from 'reactstrap';

class ModalPlaquette extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        modal: false,
        nom: ""
      };
  
      this.toggle = this.toggle.bind(this);
      this.handleChange = this.handleChange.bind(this)
    }
  
  toggle() {
    this.setState({
      modal: !this.state.modal
    });
  }

  handleChange(event) {
    const {name, value, type, checked} = event.target
    type === "checkbox" ? this.setState({ [name]: checked }) : this.setState({ [name]: value })
  }

  HoverHandler(event) {
    event.target.style = {} //Dunno why but it makes the className work !!
  }


  
    render() {
      return (
        <div>
          <div className={this.props.classe} onMouseOver={this.HoverHandler} onClick={this.toggle}>{this.props.buttonLabel}</div>

          <Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.classe}>
            <ModalHeader toggle={this.toggle}>Demander la plaquette</ModalHeader>
            <ModalBody>
              <Form>
                <Label for="nom" size="sm">Nom</Label>
                <Input 
                  type='text'
                  name="nom" 
                  id='nom'
                  placeholder="Jean Dupond"
                  onChange={this.handleChange}
                 />
                 <br/>
                <label for="mail" size="sm">Votre mail</label>
                <Input 
                  type='email'
                  name="mail"
                  id="mail"
                  onChange={this.handleChange}
                  />
              </Form>
              
            </ModalBody>
            <ModalFooter>
            
              <Button color="danger" onClick={this.toggle}>M'envoyer la plaquette par mail</Button>{' '}
              <Button color="secondary" onClick={this.toggle}>Télécharger le pdf</Button>
            </ModalFooter>
          </Modal>
        </div>
      );
    }
  }

  export default ModalPlaquette