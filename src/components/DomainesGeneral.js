import React from "react"

import * as Scroll from 'react-scroll'

import '../styles/contenu.css'


class DomainesGeneral extends React.Component {

  constructor(props) {
    super(props);
    this.state = { width: 0, height: 0 };
    this.updateWindowDimensions = this.updateWindowDimensions.bind(this);
    this.dropDown = this.dropDown.bind(this)
  }
  
  componentDidMount() {
    this.updateWindowDimensions();
    window.addEventListener('resize', this.updateWindowDimensions);
  }
  
  componentWillUnmount() {
    window.removeEventListener('resize', this.updateWindowDimensions);
  }
  
  updateWindowDimensions() {
    this.setState({ width: Math.max(1200,window.innerWidth), height: Math.max(710, window.innerHeight - 76) });
  }


  dropDown() {
    Scroll.animateScroll.scrollTo(this.state.height)
  }

  render() {
    return(
      <div className="pageContenu" style={{height:this.state.height, width:this.state.width}}>
          <h1>Nos Domaines de compétence</h1>
          <div>Praesent nec augue tellus. Proin semper augue vitae ornare vehicula. Sed congue ultrices ante, non tempor velit malesuada non. Vivamus ac sem a leo placerat pharetra vitae sed massa. Sed tempor id ipsum a euismod. Suspendisse malesuada blandit porttitor. Morbi odio dolor, tincidunt at nibh finibus, rhoncus molestie leo. Mauris gravida, enim non elementum cursus, elit justo porta purus, vitae varius lorem ante a turpis. Aenean feugiat eros a libero iaculis, non convallis enim pharetra.</div>
          <div className="filiere">
            <h3>Sciences du numérique</h3>
            <div>Un court texte décrivant la filière</div>
          </div>
          <div className="filiere">
            <h3>Sciences du numérique</h3>
            <div>Un court texte décrivant la filière</div>
          </div>
          <div className="filiere">
            <h3>Sciences du numérique</h3>
            <div>Un court texte décrivant la filière</div>
          </div>
      </div>

    ) 
  }
}

export default DomainesGeneral
