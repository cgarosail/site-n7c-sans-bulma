import React from 'react'


import Layout from '../components/layout';
import FirstImpression from '../components/FirstImpression'
import Stats from '../components/Stats'
import "../styles/index.scss"

function IndexPage(data) {
  console.log(data.pages)
  
  return (
        <Layout>
          <div className="page">
          <FirstImpression />
          <Stats />
          </div>
        </Layout>
  )
}

export default IndexPage